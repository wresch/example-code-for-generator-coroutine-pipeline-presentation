#! /usr/bin/env python

import collections
import functools
import multiprocessing
import sys
import time
import itertools
import types
import inspect
import os.path


commands = {}
def make_command(**kwargs):
    """takes annotation and adds it to the function; used
    to convert command line args for function call"""
    def _make_command(f):
        commands[f.__name__] = f
        for k in kwargs:
            setattr(f, k, kwargs[k])
        return f
    return _make_command

@make_command()
def help():
    name = os.path.basename(sys.argv[0])
    print "USAGE: %s CMD [opt=val]\n\nCommands:" % name
    for k in sorted(commands.keys()):
        print "  ", k, inspect.formatargspec(*inspect.getargspec(commands[k]))
        if commands[k].__doc__ is not None:
            print "     ", "\n      ".join(commands[k].__doc__.split("\n"))
    sys.exit()


class Tag(collections.namedtuple("Tag", "id seq qual")):
    """
    Lightweight, non-mutable object to pass along the pipeline
    stages
    """
    __slots__ = ()
    def __str__(self):
        return "@%s\n%s\n+\n%s" % self
    def __len__(self):
        return len(self.seq)


def fastq_to_tag(input):
    "transform iterable to Tags and pass them to target"
    for line in input:
        if line.startswith('@'):
            id1  = line.strip()[1:]
            seq  = input.next().strip()
            id2  = input.next().strip()[1:]
            qual = input.next().strip()
            if id2 != id1 or len(seq) != len(qual):
                raise ValueError, "error in input fastq format"
            yield Tag(id1, seq, qual)

def tabulate_base_composition(input):
    """cumulative tabulation of base composition of tags"""
    comp = collections.defaultdict(int)
    for tag in input:
        for n in tag.seq:
            comp[n] += 1
    return comp

def phred64_to_p(s):
    return [10 ** (-0.1 * (ord(x) - 64)) for x in s]

def trim(input, min_len, min_prob):
    """assumes phred64 and uses an (intentionally) naive brute
    force approach to finding the longest read of at least min_len
    that still has a probability of being correct of min_prob. the point
    here is to do more work, not be optimal"""
    product = lambda a,b: a * b 
    for tag in input:
        correct_prob = [1.0 - x for x in phred64_to_p(tag.qual)]
        tag_len = len(tag)
        left_coord = range(tag_len - min_len)
        right_coord = range(min_len, tag_len)
        possible_subsequences = [x for x in itertools.product(left_coord, right_coord) if x[1] - x[0] >= min_len]
        products = [(x[1] - x[0], reduce(product, correct_prob[x[0]:x[1]]), x[0], x[1]) for x in possible_subsequences]
        products.sort(reverse=True)
        for l, p, s, e in products:
            if p >= min_prob:
                yield Tag(tag.id, tag.seq[s:e], tag.qual[s:e])
                break
        

def print_dict(d):
    if len(d) > 0:
        max_key_len = max(len(str(x)) for x in d)
        max_val_len = max(len(str(x)) for x in d.values())
        print "\n".join(str(a).rjust(max_key_len) + ": " + str(b).rjust(max_val_len) for a, b in d.items())

Task = collections.namedtuple("Task", "f iterable args kwargs")
def process_task(input_q, result_q):
    for task in iter(input_q.get, 'STOP'):
        try:
            result = task.f(task.iterable, *task.args, **task.kwargs)
        except:
            return
        # can't pickle generator
        if type(result) == types.GeneratorType:
            result = list(result)
        result_q.put(result)


def proc_multi(f, input, nr_proc, 
        chunksize=10000, args=[], kwargs={},
        returns_iter=False):
    task_queue = multiprocessing.Queue()
    result_queue = multiprocessing.Queue()

    # fire up processes
    for i in range(nr_proc):
        multiprocessing.Process(target=process_task,
                args=(task_queue, result_queue)).start()
    
    # feed the queue
    chunk = []
    submitted_tasks = 0
    for i in input:
        chunk.append(i)
        if len(chunk) == chunksize:
            task_queue.put(Task(f, chunk, args, kwargs))
            submitted_tasks += 1
            chunk = []
    if chunk != []:
        task_queue.put(Task(f, chunk, args, kwargs))
        submitted_tasks += 1

    # collect the results
    for i in range(submitted_tasks):
        result = result_queue.get()
        if returns_iter:
            try:
                for j in result:
                    yield j
            except TypeError:
                return
        else:
            yield result

    # tell processes to stop
    for i in range(nr_proc):
        task_queue.put('STOP')


def combine_dicts(input):
    total = collections.defaultdict(int)
    for d in input:
        for k in d:
            total[k] += d[k]
    return total

@make_command(filename=str)
def sequential(filename="test.fq"):
    "run sequential pipeline - base comp"
    print "sequential pipeline [base comp] running"
    start = time.time()

    tags = fastq_to_tag(open(filename))
    comp = tabulate_base_composition(tags)
    print_dict(comp)

    end = time.time()
    print "   %.3fs" % (end-start)

@make_command(filename=str)
def sequential2(filename="test_short.fq"):
    "run sequential pipeline - trim reads"
    print "sequential pipeline [trimming] running"
    start = time.time()
    
    with open("trim.out", "w") as out:
        tags = fastq_to_tag(open(filename))
        trimmed_tags = trim(tags, min_len=25, min_prob=0.90)
        for tag in trimmed_tags:
            print >>out, tag

    end = time.time()
    print "   %.3fs" % (end-start)

@make_command(n=int, filename=str)
def parallel(n=2, filename="test.fq"):
    "run parallel pipeline - base comp"
    print "parallel pipeline [base comp] running (n=%i)" % n
    start = time.time()

    tags = fastq_to_tag(open(filename))
    comp_dicts = proc_multi(tabulate_base_composition, tags, n)
    comp = combine_dicts(comp_dicts)
    print_dict(comp)
    
    end = time.time()
    print "   %.3fs" % (end-start)

@make_command(n=int, filename=str)
def parallel2(n=2, filename="test_short.fq"):
    "run parallel pipeline - trim reads"
    print "parallel pipeline [trimming] running (n=%i)" % n
    start = time.time()
    
    with open("trim.out", "w") as out:
        tags = fastq_to_tag(open(filename))
        trimmed_tags = proc_multi(trim, tags, n, 
                kwargs={"min_len": 25, "min_prob": 0.90},
                returns_iter=True)
        for tag in trimmed_tags:
            print >>out, tag

    end = time.time()
    print "   %.3fs" % (end-start)

if __name__ == "__main__":
    if len(sys.argv) >= 2:
        if sys.argv[1] in commands:
            # parse the kwargs from command line
            f = commands[sys.argv[1]]
            if len(sys.argv) > 2:
                args = [x.split("=") for x in sys.argv[2:]]
                args = [(k.strip(), v.strip()) for k,v in args]
                kwargs = dict((k, getattr(f, k)(v)) for k,v in args)
            else:
                kwargs = {}
            commands[sys.argv[1]](**kwargs)
            sys.exit(0)
    help()
    sys.exit(1)
